package com.felipe.factorypattern

class Sliced: Bread {
    override fun name(): String = "Pan de molde"

    override fun calories(): String = " : 107 kcal"
}