package com.felipe.abstractfactorypattern

interface Filling {
    fun name(): String
    fun calories(): String
}