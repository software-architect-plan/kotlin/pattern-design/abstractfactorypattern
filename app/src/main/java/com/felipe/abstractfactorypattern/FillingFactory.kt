package com.felipe.abstractfactorypattern

import com.felipe.factorypattern.Bread

class FillingFactory: AbstractFactory() {
    override fun getBread(breadType: String?): Bread? = null

    override fun getFilling(fillingType: String?): Filling? {
        if (fillingType == null) return null

        return when (fillingType) {
            "CHE" -> Cheese()
            "TOM" -> Tomato()
            "HAM" -> Ham()
            else -> null
        }
    }

    override fun getDrink(drinkType: String?): Drink? = null
}