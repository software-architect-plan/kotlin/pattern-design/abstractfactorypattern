package com.felipe.abstractfactorypattern

interface Drink {
    fun name(): String
    fun calories(): String
}