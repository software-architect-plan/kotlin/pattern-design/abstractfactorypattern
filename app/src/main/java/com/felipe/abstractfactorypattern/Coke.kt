package com.felipe.abstractfactorypattern

class Coke: Drink {
    override fun name(): String = "Coca Cola"

    override fun calories(): String = " : 139 kcal"
}