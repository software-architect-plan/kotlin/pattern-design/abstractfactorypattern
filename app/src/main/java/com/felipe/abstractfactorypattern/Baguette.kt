package com.felipe.factorypattern

class Baguette: Bread {
    override fun name(): String {
        return "Baguette"
    }

    override fun calories(): String = " : 238 kcal"
}