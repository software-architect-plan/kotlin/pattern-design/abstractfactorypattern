package com.felipe.factorypattern

class Roll: Bread {
    override fun name(): String = "Pan de pita"

    override fun calories(): String = " : 245 kcal"
}