package com.felipe.factorypattern

interface Bread {
    fun name(): String
    fun calories(): String
}
