package com.felipe.abstractfactorypattern

class Ham: Filling {
    override fun name(): String = "Jamón"

    override fun calories(): String = " : 195 kcal"
}