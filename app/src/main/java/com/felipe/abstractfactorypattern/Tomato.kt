package com.felipe.abstractfactorypattern

class Tomato: Filling {
    override fun name(): String = "Tomate"

    override fun calories(): String = " : 22 kcal"
}