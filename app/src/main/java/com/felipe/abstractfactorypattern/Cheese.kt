package com.felipe.abstractfactorypattern

class Cheese: Filling {
    override fun name(): String = "Queso"

    override fun calories(): String = " : 100 kcal"
}