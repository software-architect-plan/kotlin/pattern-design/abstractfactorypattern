package com.felipe.abstractfactorypattern

class Beer: Drink {
    override fun name(): String = "Cerveza"

    override fun calories(): String = " : 155 kcal"
}