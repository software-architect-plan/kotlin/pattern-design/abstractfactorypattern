package com.felipe.abstractfactorypattern

import com.felipe.factorypattern.Baguette
import com.felipe.factorypattern.Bread
import com.felipe.factorypattern.Roll
import com.felipe.factorypattern.Sliced

class BreadFactory: AbstractFactory() {
    override fun getBread(breadType: String?): Bread? {
        if (breadType == null) return null

        return when (breadType) {
            "BAG" -> Baguette()
            "ROL" -> Roll()
            "SLI" -> Sliced()
            else -> null
        }
    }

    override fun getFilling(fillingType: String?): Filling? = null
    override fun getDrink(drinkType: String?): Drink? = null
}